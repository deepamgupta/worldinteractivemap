import { HttpClient } from "@angular/common/http";
import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import { xlsService } from '../services/xls.service';
import { EnvService } from '../services/env.service';
import { take, tap } from "rxjs/operators";
import { MatTable, MatTableDataSource } from "@angular/material/table";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort, Sort } from "@angular/material/sort";

export interface dataType1 {
  Country: string;
}

@Component( {
  selector: "app-map-data",
  templateUrl: "./map-data.component.html",
  styleUrls: [ "./map-data.component.css" ],
  providers: [ xlsService, EnvService ]
} )
export class MapDataComponent implements OnInit {
  xlsFileData: any;
  columnsDefined: Boolean = false;

  displayedColumns: string[] = [];
  columnSelection: any[] = [];
  dataSource = new MatTableDataSource<any>( [] );


  @ViewChild( MatPaginator, { static: true } ) paginator: MatPaginator;
  @ViewChild( MatTable ) table: MatTable<any>;
  @ViewChild( MatSort ) sort: MatSort;

  constructor( private http: HttpClient,
               private xlxs: xlsService,
               private env: EnvService,
               public changeDetectorRef: ChangeDetectorRef
  ) {
  }

  /**
   * Gets all data from xls file into local variable as json
   */
  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.http.get( this.env.dataPath, { responseType: 'blob' } )
      .pipe(
        tap(
          ( data: any ) => {
            this.xlsFileData = this.xlxs.getJsonFromXlsx( data );
          }
        ),
        take( 1 )
      )
      .subscribe();

  }

  get columnSelectionForDisplay() {
    return this.columnSelection.filter( f => f.show === true ).map( m => {
      return m.name
    } );
  }

  /**
   * Extracts all headers into an array
   * @private
   */
  private updateColumnHeaders() {
    this.columnsDefined = true;
    const dataArray = this.xlsFileData[ '__zone_symbol__value' ];
    const columnsfiltered = dataArray.filter( x => x[ Object.keys( x )[ 0 ] ] === 'Country' );
    this.displayedColumns = Object.values( columnsfiltered[ 0 ] );

    // If column header is blank then set its index in place of the header
    // as we cant have blank headers
    this.displayedColumns.forEach( ( item, index ) => {
      if( item === '' || item === undefined || item === null ) {
        this.displayedColumns[ index ] = index.toString();
      }
    } )
  }

  /**
   * Updates given feature's data from xls/ json into table datasource
   * @param feature
   */
  public update( feature ) {
    this.updateColumnHeaders();

    const dataArray = this.xlsFileData[ '__zone_symbol__value' ];
    const datafiltered = dataArray.filter( x => x[ Object.keys( x )[ 0 ] ] === feature.properties.name );

    this.mergeColumnsWithData( datafiltered );

    // Removed all empty columns only when there is at least one row
    // otherwise you see a empty table with no rows and no columns
    if( this.dataSource.data.length > 0 )
      this.removeEmptyColumns();

    this.columnSelection = this.displayedColumns.map( c => {
      return { name: c, show: true }
    } );
    this.changeDetectorRef.detectChanges();
  }

  /**
   * Merges the data with column names and updates
   * the datasource of the table
   * @private
   */
  private mergeColumnsWithData( datafiltered ) {
    const dataFilteredWithColumns: any[] = []

    // Merge columns and its data into one array
    datafiltered.forEach( ( item ) => {
      let dataInColumns: any = {};
      this.displayedColumns.forEach( ( key, i ) => {
        const itemkey = Object.keys( item )[ i ];
        dataInColumns[ key ] = item[ itemkey ];
      } );
      dataFilteredWithColumns.push( dataInColumns );
    } )

    this.dataSource.data = dataFilteredWithColumns;
  }

  /**
   * Removes columns that have no data
   * @private
   */
  private removeEmptyColumns() {
    const columns = {};

    this.displayedColumns.forEach( col => {
      columns[ col ] = this.dataSource.data.every( element => {
        return !element[ col ];
      } );
    } );
    this.displayedColumns = this.displayedColumns.filter( col => !columns[ col ] );

  }

  /**
   * compares values and swaps them
   * @param a
   * @param b
   * @param isAsc
   * @private
   */
  private compare( a: number | string, b: number | string, isAsc: boolean ) {
    return ( a < b ? -1 : 1 ) * ( isAsc ? 1 : -1 );
  }

  /**
   * Sort event
   * @param sort
   */
  sortData( sort: Sort ) {
    const data = this.dataSource.data;
    if( !sort.active ) {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort( ( a, b ) => {
      const isAsc = sort.direction === 'asc';
      return this.compare( a[ sort.active ], b[ sort.active ], isAsc );
    } );
  }

  /**
   * applies filter on all texts of all columns
   * @param event
   */
  applyFilter( event: Event ) {
    const filterValue = ( event.target as HTMLInputElement ).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.changeDetectorRef.detectChanges();
  }

  /**
   * Hide and displays selected column
   * @param column
   */
  setColumnVisibility( column, event ) {
    this.columnSelection.filter( f => f.name === column.name ).forEach( ( item ) => {
      item.show = !column.show;
    } );
  }

}




import { Component, ElementRef, EventEmitter, Output, ViewChild } from "@angular/core";
import "leaflet/dist/leaflet.css";
import { latLng, tileLayer } from "leaflet";

import * as L from "leaflet";
import { HttpClient } from "@angular/common/http";
import { xlsService } from '../services/xls.service';
import { EnvService } from '../services/env.service';

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.css"],
  providers:[xlsService, EnvService]
})
export class MapComponent {

  @Output() countrySelected:EventEmitter<any> = new EventEmitter();


  options = {
    layers: [
      tileLayer("https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png",{
        maxZoom: 2.3,
        minZoom: 2.3,
        noWrap: true,
      })
    ],
    zoom: 1,
    zoomControl: false,
    center: latLng(49.3824, 18.1055),
  };

  constructor(private http: HttpClient) {
  }

  /**
   * Style when a country is hovered on
   * @param e
   */
  highlightCountry(e) {
    var layer = e.target;

    layer.setStyle({
      weight: 1,
      fillColor: "#009900",
      dashArray: "",
      fillOpacity: 7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
      layer.bringToFront();
    }
    e.target.openPopup(e.latlng);
  }

  /**
   * Resets the style back to normal map
   * @param e
   */
  resetHighlightCountry(e) {
    L.geoJSON().resetStyle(e.target);
    e.target.setStyle({ fillColor: '#DEF0CE',fillOpacity:1, color:"grey",weight:0.8 });
    e.target.closePopup();
  }

  /**
   * Event when a country is clicked
   * @param event
   */
  clicked = (event: any) => {
    this.countrySelected.emit(event.target);
  }

  /**
   * Once the map is ready, it pans to the user's current location and loads the map.geojson
   * @param map Map instance
   */
  onMapReady(map: L.Map) {
    map.dragging.disable();
    map.scrollWheelZoom.disable();
    map.doubleClickZoom.disable();
    map.attributionControl.remove();

    const onEachFeature = (feature, layer) => {

      const popupContent = feature.properties.name;
      layer.bindPopup(popupContent);

      layer.on({
        mouseover: this.highlightCountry,
        mouseout: this.resetHighlightCountry,
        click: this.clicked,

      });

    };

    this.http.get("assets/countries.json").subscribe((json: any) => {

       L.geoJSON(json,{
        onEachFeature: onEachFeature,
        style: function(feature) {
          return { fillColor: '#DEF0CE',fillOpacity:1, color:"grey",weight:0.8 };
        }
       }).addTo(map);


    });
  }


}

import { AfterViewInit, Component, ElementRef, ViewChild } from "@angular/core";
import { MapDataComponent } from '../map-data/map-data.component';

@Component( {
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: [ "./app.component.css" ],
  providers: []
} )
export class AppComponent {

  @ViewChild( MapDataComponent ) mapData: MapDataComponent;

  countrySelected( target ) {
    this.mapData.update( target.feature );
    this.scrollToBottom();

  }

  scrollToBottom(): void {
    const scrollingElement = ( document.scrollingElement || document.body );

    scrollingElement.scroll( {
      top: scrollingElement.scrollHeight,
      left: 0,
      behavior: 'smooth'
    } );
  }
}

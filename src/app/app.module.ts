import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { MapComponent } from '../map.component/map.component';
import { MapDataComponent } from '../map-data/map-data.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSortModule } from "@angular/material/sort";
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';

@NgModule( {
  declarations: [ AppComponent, MapComponent, MapDataComponent ],
  imports: [ BrowserModule, LeafletModule, HttpClientModule, BrowserAnimationsModule,
    MatTableModule, NgbModule, MatSortModule, MatFormFieldModule, MatInputModule, MatMenuModule, MatIconModule   ],
  providers: [],
  bootstrap: [ AppComponent ]
} )
export class AppModule {
}

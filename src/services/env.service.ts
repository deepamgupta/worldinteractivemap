import { Injectable } from '@angular/core';
@Injectable()
export class EnvService {

  // These variables are default values for the application, but can be overwritten by variables in env.js (if env.js exists).

  public production = false;


  public dataPath = 'assets/Local Legislation Review.xlsx';

  constructor() {
  }

}

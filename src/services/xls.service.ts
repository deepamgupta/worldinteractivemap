import { Injectable } from "@angular/core";
import * as XLSX from 'xlsx';

/**
 * Interfaces with the api
 * Converts xls data into json
 */
@Injectable()
export class xlsService {

  getJsonFromXlsx(file: File): any {
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer( file );

    return new Promise((resolve,reject)=>{
    fileReader.onload = async ( e ) => {
      let arrayBuffer: any = fileReader.result;
      const data = new Uint8Array( arrayBuffer );
      var arr = new Array();
      for( var i = 0; i != data.length; ++i )
        arr[ i ] = String.fromCharCode( data[ i ] );
      const bstr = arr.join( "" );
      const workbook = XLSX.read( bstr, { type: "binary" } );
      const first_sheet_name = workbook.SheetNames[ 0 ];
      const worksheet = workbook.Sheets[ first_sheet_name ];
      await resolve(XLSX.utils.sheet_to_json( worksheet, { raw: false, defval:"" } ) );
    }
  })}

}
